import React, { useContext, useEffect, useRef } from 'react';
import classnames from 'classnames';
import FormControl from 'react-bootstrap/FormControl';

import { FormContext } from './Form';
import { LabelFormContext } from './Label';
import { FormGroupContext } from './FormGroup';

function Select({
	as,
	children,
	className,
	disabled,
	onChange: propOnChange,
	type,
	validations,
	value,
	...props
}) {
	const selectElem = useRef(null);
	const formState = useContext(FormContext);
	const groupState = useContext(FormGroupContext);

	const disable = disabled || formState.submitting;
	const name = groupState.name || groupState.controlId;
	const id = groupState.controlId;

	useEffect(() => {
		if (value === undefined) formState.setField(name, selectElem.current.value);
	}, [value]);
	useEffect(() => formState.register(name, validations), [name]);

	function onChange(event) {
		const target = event.target || {};
		formState.setField(target.name, target.value);
		formState.validate(target.name, target.value, validations);
		propOnChange && propOnChange(event);
	}

	return (
		<select
			disabled={disable}
			onChange={onChange}
			name={name}
			id={id}
			type={type}
			value={value}
			ref={selectElem}
			{...props}
			className={classnames(className, {
				'is-invalid': formState.needsValidation && groupState.error,
			})}
		>
			{children}
		</select>
	);
}

export default Select;
