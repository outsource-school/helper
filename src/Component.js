import React from 'react';

function Component() {
	return (
		<div>
			<h1>@outsource-school/helper Testing Component</h1>
		</div>
	);
}

export default Component;
