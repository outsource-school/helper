import React from 'react';

export const ManifestContext = React.createContext();
function ComponentManifest({ manifest, children }) {
	return <ManifestContext.Provider value={manifest}>{children}</ManifestContext.Provider>;
}

export default ComponentManifest;
