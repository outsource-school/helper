import React, { useContext } from 'react';
import Alert from 'react-bootstrap/Alert';
import get from 'lodash/get';
import isString from 'lodash/isString';
import { FormContext } from './Form';

function HttpError({ useForm, header, error, reload, ...props }) {
	const form = useContext(FormContext);
	const locale = String;
	const formError = useForm && form.httpError;
	const connectionError = !get(error, 'response.status') || get(error, 'response.status') >= 500;
	if (!error && !formError) return null;

	const { response } = formError || error;
	const stack = get(response, 'data.error.stack') || get(error, 'stack');
	const info = get(response, 'data.info.message') || get(error, 'message');
	const body = get(response, 'data');

	if (connectionError && reload)
		return (
			<button className="btn btn-block" onClick={reload}>
				<span>Connection Error, click here to retry</span>
				<i className="ml-3 fal fa-sync"></i>
			</button>
		);

	return (
		<Alert variant="warning" {...props}>
			<Alert.Heading>
				{header || locale('There was an issue connecting to OutsourceSchool')}
			</Alert.Heading>
			<hr />
			<p>{locale('The full error is displayed below', { response })}</p>
			<p>{isString(body) && body}</p>
			{info && <p>{info}</p>}
			{stack && <pre>{stack}</pre>}
		</Alert>
	);
}

export default HttpError;
