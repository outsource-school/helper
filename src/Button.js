import React, { useContext } from 'react';
import BsButton from 'react-bootstrap/Button';

import { FormContext } from './Form';
import Timer from './Timer';

function Button({ children, disabled, showSpinner, ...props }) {
	const formState = useContext(FormContext);
	const disable = disabled || formState.hasError || formState.submitting;

	return (
		<BsButton disabled={disable} {...props}>
			{children}
			{showSpinner && formState.submitting && (
				<Timer time={100}>
					<i className="ml-1 fas fa-sync-alt fa-spin"></i>
				</Timer>
			)}
		</BsButton>
	);
}

export default Button;
