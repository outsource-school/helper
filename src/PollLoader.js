import React from 'react';
import { useState, useEffect } from 'react';
import { from, zip, Subject } from 'rxjs';
import { tap, mergeMap, delay, filter } from 'rxjs/operators';
import noop from 'lodash/noop';
import flatten from 'lodash/flatten';
import { isSubscribable } from '../utils/rxjs_utils';

function PollLoader({ services, interval, resolver, children }) {
	const [resolved, setResolved] = useState(<div />);
	useEffect(() => {
		const subject = new Subject();
		const stream = subject.pipe(
			mergeMap(() => {
				const arraySafe = [].concat(services).map(srv => srv());
				const serviceObs = flatten(arraySafe)
					.filter(isSubscribable)
					.map(s => from(s));
				return zip(...serviceObs);
			}),
			filter(data => {
				const resolved = resolver(data);
				setResolved(resolved);
				return resolved;
			}),
			delay(interval),
			tap(() => subject.next())
		);
		const sub = stream.subscribe(noop, noop);
		subject.next();

		return () => sub.unsubscribe();
		// eslint-disable-next-line
	}, []);

	return resolved || children;
}
PollLoader.defaultProps = {
	children: [],
	interval: 1000,
	resolver() {},
	services: [],
};

export default PollLoader;
