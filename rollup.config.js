import autoprefixer from 'autoprefixer';
import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import filesize from 'rollup-plugin-filesize';
import generatePackageJson from 'rollup-plugin-generate-package-json';
import globby from 'globby';
import localResolve from 'rollup-plugin-local-resolve';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import postcss from 'rollup-plugin-postcss';
import resolve from 'rollup-plugin-node-resolve';
import sourcemaps from 'rollup-plugin-sourcemaps';

const pkg = require('./package');

const name = pkg.moduleName;

const config = {
	external: ['react', 'react-dom', 'react-router'],
	plugins: [
		generatePackageJson(),
		peerDepsExternal(),
		postcss({ extract: true, plugins: [autoprefixer] }),
		babel({ exclude: 'node_modules/**' }),
		localResolve(),
		resolve(),
		commonjs({
			include: 'node_modules/**',
			presets: ['react', 'react-dom', 'react-router'],
			namedExports: {
				'node_modules/react-is/index.js': ['isValidElementType', 'ForwardRef'],
				'node_modules/prop-types/index.js': ['elementType'],
			},
		}),
		filesize(),
		sourcemaps(),
	],
};

const configs = globby.sync('src/index.js').map(inputFile => ({
	...config,
	input: inputFile,
	output: [
		{ file: inputFile.replace('src', 'dist/cjs'), format: 'cjs', name, sourcemap: true },
		// { file: inputFile.replace('src', 'dist/umd'), format: 'umd', name, sourcemap: true },
		{ file: inputFile.replace('src', 'dist/esm'), format: 'esm', sourcemap: true },
	],
	format: 'umd',
}));

export default configs;